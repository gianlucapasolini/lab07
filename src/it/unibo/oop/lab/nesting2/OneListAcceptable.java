package it.unibo.oop.lab.nesting2;

import java.util.ArrayList;
import java.util.List;

public class OneListAcceptable<T> implements Acceptable<T> {

	private List<T> list;
	
	public Acceptor<T> acceptor() {
		return new OneListAcceptator();
	}

	public OneListAcceptable(List<T> list) {
		this.list = new ArrayList<T>(list);
	}
	
	public class OneListAcceptator implements Acceptor<T>{
		private List<T> list2;
		
		OneListAcceptator(){
			this.list2 = new ArrayList<T>();
		}

		public void accept(T newElement) throws ElementNotAcceptedException {
			// TODO Auto-generated method stub
			if(list.contains(newElement)) {
				list2.add(newElement);
			}
			else {
				throw new ElementNotAcceptedException((Object)newElement);
			}
		}

		public void end() throws EndNotAcceptedException {
			// TODO Auto-generated method stub
			if(list.size() != this.list2.size()) {
				throw new EndNotAcceptedException();
			}
		}
		
	}

}
